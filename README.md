# Sprich concept

## Components

The system consists of the following components:

Running on client:
- `sprich-client`: A native client written in C that sends and receives audio data to and from a `sprich-server` instance. Runs on each user's machine. Code is in the `sprich` subproject: https://gitlab.com/sprich/sprich

Running on sprich-server:
- `sprich-server`: A native server written in C that exchanges audio data between multiple `sprich-client` instances. Code is in the `sprich` subproject: https://gitlab.com/sprich/sprich

Running on a (not necessarily) seperate server:
- `kontrollier`: A websocket server (the "backend") that is used mainly to tell `sprich-client` instances the network address (i.e. IP address and UDP port) where they should send and receive their audio data to/from and which audio devices they should use. `sprich-client` and `fesch` instances connect to this and use it to communicate. Subproject: https://gitlab.com/sprich/kontrollier
- `fesch`: An HTTP service (the "frontend") that serves a web page that users can use to control their locally-running `sprich-client` instance (i.e., choose which mic and which speakers to use). This also spits out the websocket URL of the `kontrollier` instance that users need to feed into their locally-running `sprich-client` so it knows where to connect.

## A basic use case

User U wants to join a `sprich` voice call. To do so, they:
1. Navigate to the `fesch` web site in their web browser. That web site connects the web browser to the `kontrollier` instance via a websocket connection.
2. Copy the websocket URL displayed on the web site (which points to the `kontrollier` instance the browser just connected to in step `1`).
3. Open the native `sprich-client`, i.e. run their local copy of the `sprich-client` binary on their local machine.
4. Paste the websocket URL copied in step `2` into the text field displayed by the `sprich-client` window and click `connect`. The `sprich-client` then connects to that `kontrollier` instance.
5. When the `sprich-client` successfully connects to the `kontrollier` instance (whose websocket URL the user just pasted in step `4`), this is reflected both in the `sprich-client`'s window and the `fesch` web site displayed in the web browser.
6. The `sprich-client` receives the network address of the `sprich-server` instance from the `kontrollier` instance and starts sending and receiving audio data to/from the `sprich-server` via UDP while using the default audio input/output devices. Also, the `sprich-client` sends the lists of available audio inputs and outputs (i.e. microphones and speakers) to the `kontrollier` instance which in turn passes those lists along to the `fesch` web site displayed in the user's web browser.
7. The user can now change the used microphone and speaker in the `fesch` web site and any changes they make are sent to the `kontrollier` instance via the websocket connection and passed back to the connected `sprich-client` instance.

![](images/info_connection.png)
![](images/info_data.png)
